public class Converter 
{
	/* Names go here: 
	
	@ Author:	Rebecca Reid
				Kevin Lici
				Ines Rosito
	*/
	
	private double celsiusToFahrenheit(double C)
	{
		// TODO: third student will implement this method:
		return (C * 9 / 5) + 32;
	}
	
	
	private double fahrenheitToCelsius(double F)
	{
		// TODO: second student implements this method 
		return (F - 32) * 5 / 9;
	}
	
	public static void main(String[] args)
	{
		// TODO: The first student will implement this method.
		// Call CelsiusToFahrenheit to convert 180 Celsius to Fahrenheit value.
		System.out.print("Converting 180 Celsius to Farenheit. Result should be 356 Farenheit. Result is :  ");
		System.out.println(celsiusToFahrenheit(180));
		// Call FahrenheitToCelsius to convert 250 Fahrenheit to Celsius value.
		System.out.print("Converting 250 Fahrenheit to Celsius. Result should be 121.111 Celsuis. Result is :  ");
		System.out.println(fahrenheitToCelsius(250));
	}
	
}